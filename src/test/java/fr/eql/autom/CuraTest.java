package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class CuraTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver=new ChromeDriver();

    }
    @Test
    public void test(){
        driver.get("https://katalon-demo-cura.herokuapp.com/");
        driver.manage().window().maximize();
        //implicit wait
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //explicit wait
        WebDriverWait wait = new WebDriverWait(driver, 3);


        By makeAppointmentBy= new By.ById("btn-make-appointment");
        By usernameBy = new By.ById("txt-username");
        By passwordBy = new By.ById("txt-password");
        By loginBy = new By.ById("btn-login");

        //erreur volontaire pour provoquer les timeout
        By menuToggleBy = new By.ById("menu-touggle");
        By logoutBy = new By.ByLinkText("Logout");

        //fluent wait
        Wait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
                .withTimeout((Duration.ofSeconds(12)))
                .pollingEvery(Duration.ofMillis(200))
                .ignoring(NoSuchElementException.class);

        //sign in
        WebElement makeAppointment = driver.findElement(makeAppointmentBy);
        makeAppointment.click();
        WebElement username = driver.findElement(usernameBy);
        WebElement password = driver.findElement(passwordBy);
        username.sendKeys("John Doe");
        password.sendKeys("ThisIsNotAPassword");
        //fluent wait
        WebElement loginBtn = wait.until((new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver webDriver) {
                return driver.findElement(loginBy);
            }
        }));

        //WebElement loginBtn = driver.findElement(loginBy);
        loginBtn.click();

        //sign out
        //explicit wait
        wait.until(ExpectedConditions.elementToBeClickable(menuToggleBy));
        WebElement menuToggle = driver.findElement(menuToggleBy);
        menuToggle.click();
        WebElement logOut = driver.findElement(logoutBy);
        logOut.click();
    }

    @After
    public void tearDown(){
        //driver.quit();
    }
}
