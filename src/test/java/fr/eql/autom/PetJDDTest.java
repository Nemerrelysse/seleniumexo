package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PetJDDTest {
    WebDriver driver;
    String url = "https://petstore.octoperf.com/actions/Catalog.action";
    String dog="Dalmation";
    String username;
    String password;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws IOException {
        //locators
        By signInBy=By.partialLinkText("Sign In");
        By passwordInputBy=By.name("password");
        By usernameInputBy=By.name("username");
        By logInBy=By.name("signon");
        By signOutBy=By.partialLinkText("Sign Out");
        By welcomeBy=By.id("WelcomeContent");
        By titleBy=By.xpath("//div[@id='Catalog']/h2");

        //load jdd
        List<Map<String,String>> jdd=loadCSVJDD("petStoreConnect.csv");
        username=jdd.get(0).get("username");
        System.out.println(username);
        password=jdd.get(0).get("password");
        System.out.println(password);

        //log in test
        driver.get(url);
        driver.manage().window().maximize();
        WebElement singIn = driver.findElement(signInBy);
        singIn.click();
        WebElement passwordInput = driver.findElement(passwordInputBy);
        passwordInput.clear();
        WebElement usernameInput = driver.findElement(usernameInputBy);
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);
        WebElement logIn = driver.findElement(logInBy);
        logIn.click();
        WebElement signOut = driver.findElement(signOutBy);
        assertTrue(signOut.isDisplayed());
        WebElement welcome = driver.findElement(welcomeBy);
        assertTrue(welcome.isDisplayed());
        assertEquals("Welcome ABC!",welcome.getText());

        //go to dog category test
        goToCategory("dogs");

        //go to the choosen dog page
        WebElement cell=getCell(getRowIndex(dog),1);
        WebElement dogLink=cell.findElement(By.xpath("a"));
        dogLink.click();

        //dog page test
        WebElement title= driver.findElement(titleBy);
        assertEquals(dog,title.getText());

        //write the dog in a .txt
        textFileWriting("src/test/ressources/outputs/dog.txt",dog);

    }

    @After
    public void tearDown(){
        driver.quit();
    }

    //go to a category (not case-sensitive)
    public void goToCategory(String choice){
        String upperChoice=choice.toUpperCase();
        WebElement category = driver.findElement(By.xpath("//div[@id='SidebarContent']/a[@href='/actions/Catalog.action?viewCategory=&categoryId="+upperChoice+"']"));
        category.click();
        String listChoice=upperChoice.charAt(0)+choice.substring(1).toLowerCase();
        WebElement list = driver.findElement(By.xpath("//div[@id='Catalog']/h2[text()='"+listChoice+"']/following-sibling::table"));
        assertTrue(list.isDisplayed());
    }

    //get the first row that contains the string (case-sensitive)
    public int getRowIndex(String s){
        int currentline=1;
        List<WebElement> rows = driver.findElements(By.xpath("//table/tbody/tr"));
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.xpath("td"));
            for (WebElement cell : cells) {
                if(cell.getText().equals(s)) {
                    return currentline;
                }
            }
            currentline++;
        }
        return -1;
    }

    //get the td cell at the given row and column (index starts at 1)
    public WebElement getCell(int row, int col){
        WebElement cell = driver.findElement(By.xpath("//table/tbody/tr["+row+"]/td["+col+"]"));
        return cell;
    }

    //load a csv jdd
    public ArrayList<Map<String,String>> loadCSVJDD (String fileName) throws IOException {
        String csvFilePath ="src/test/ressources/jdd/"+fileName;
        ArrayList<Map<String,String>> listJDD = new ArrayList<>();
        List<String[]> list=
                Files.lines(Paths.get(csvFilePath))
                        .map(line ->line.split("\\\\r?\\\\n"))
                        .collect(Collectors.toList());
        String[] titres = list.get(0)[0].split(",");
        for (int i=1;i<list.size();++i){
            Map<String,String> jdd =new HashMap<>();
            String[] val =list.get(i)[0].split(",");
            for (int j=0; j<titres.length;++j){
                jdd.put(titres[j],val[j]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }

    //write in a txt file
    public void textFileWriting(String file, String text){
        try {
            FileOutputStream outputStream=new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter=new OutputStreamWriter(outputStream,"UTF-8");
            BufferedWriter bufferedWriter=new BufferedWriter((outputStreamWriter));
            bufferedWriter.write(text);
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("Problème avec le fichier "+file);
            throw new RuntimeException(e);
        }
    }
}
