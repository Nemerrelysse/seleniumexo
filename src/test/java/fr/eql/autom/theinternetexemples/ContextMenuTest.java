package fr.eql.autom.theinternetexemples;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class ContextMenuTest {
    String url ="https://the-internet.herokuapp.com/";
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test(){
        By contextMenuLinkBy = By.linkText("Context Menu");
        By titleBy = By.xpath("//h3");
        By clickZoneBy =By.id("hot-spot");

        Actions a =new Actions(driver);
        WebDriverWait wait=new WebDriverWait(driver,10);

        //open page
        driver.get(url);
        driver.manage().window().maximize();

        //got to the context menu page
        WebElement contextMenuLink = driver.findElement(contextMenuLinkBy);
        contextMenuLink.click();

        //assert we are on the context menu page
        WebElement title = driver.findElement(titleBy);
        assertEquals("Context Menu", title.getText());

        //right click on the zone
        WebElement clickZone= driver.findElement(clickZoneBy);
        a.contextClick(clickZone).build().perform();

        //assert the alert is displayed
        Alert alert=wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert();
        assertEquals("You selected a context menu",alert.getText());
        alert.accept();
    }

    @After
    public void tearDown(){
        driver.quit();
    }


}
