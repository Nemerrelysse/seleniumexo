package fr.eql.autom.theinternetexemples;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class JSAlertTest {
    String url ="https://the-internet.herokuapp.com/";
    WebDriver driver;
    String category="JavaScript Alerts";
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test(){
        By contextMenuLinkBy = By.linkText(category);
        By titleBy = By.xpath("//h3");
        By alert1By = By.xpath("//button[text()='Click for JS Alert']");
        By alert2By = By.xpath("//button[text()='Click for JS Confirm']");
        By alert3By = By.xpath("//button[text()='Click for JS Prompt']");
        By resultBy = By.id("result");

        WebDriverWait wait=new WebDriverWait(driver,10);

        //open page
        driver.get(url);
        driver.manage().window().maximize();

        //got to the context menu page
        WebElement contextMenuLink = driver.findElement(contextMenuLinkBy);
        contextMenuLink.click();

        //assert we are on the context menu page
        WebElement title = driver.findElement(titleBy);
        assertEquals(category, title.getText());

        //click on the first alert
        WebElement alert1Button = driver.findElement(alert1By);
        alert1Button.click();

        //assert the alert is displayed
        Alert alert=wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert();
        assertEquals("I am a JS Alert",alert.getText());
        alert.accept();

        driver.switchTo().defaultContent();

        //click on the 2nd alert
        WebElement alert2Button = driver.findElement(alert2By);
        alert2Button.click();

        //assert the alert is displayed
        alert=wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert();
        assertEquals("I am a JS Confirm",alert.getText());

        //cancel and check if the result matches
        alert.dismiss();
        WebElement result = driver.findElement(resultBy);
        assertEquals("You clicked: Cancel",result.getText());

        //click on the third alert
        WebElement alert3Button = driver.findElement(alert3By);
        alert3Button.click();

        //assert the alert is displayed
        alert=wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert();
        assertEquals("I am a JS prompt",alert.getText());

        //send "coucou" and check result
        alert.sendKeys("coucou");
        alert.accept();
        result = driver.findElement(resultBy);
        assertEquals("You entered: coucou",result.getText());


    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
