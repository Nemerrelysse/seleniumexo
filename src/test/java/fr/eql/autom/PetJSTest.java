package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class PetJSTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test(){
        //log in test
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
        WebElement singIn = driver.findElement(By.partialLinkText("Sign In"));
        highlightClick(singIn);
        WebElement passwordInput = driver.findElement(By.name("password"));
        passwordInput.clear();
        WebElement usernameInput = driver.findElement(By.name("username"));
        highlightSendKeys(usernameInput,"j2ee");
        highlightSendKeys(passwordInput,"j2ee");
        WebElement logIn = driver.findElement(By.name("signon"));
        highlightClick(logIn);
        WebElement signOut = driver.findElement(By.partialLinkText("Sign Out"));
        assertTrue(signOut.isDisplayed());
        WebElement welcome = driver.findElement(By.id("WelcomeContent"));
        assertTrue(welcome.isDisplayed());
        assertEquals("Welcome ABC!",welcome.getText());

        //open my account
        WebElement myAccount = driver.findElement(By.partialLinkText("My Account"));
        highlightClick(myAccount);
        WebElement userInformation = driver.findElement(By.xpath("//h3[text()='User Information']"));
        assertTrue(userInformation.isDisplayed());

        //change selected language
        WebElement language = driver.findElement(By.name("account.languagePreference"));
        Select languageSelect = highLightSelect(language,"japanese");
        assertFalse(languageSelect.isMultiple());
        String selectedLanguage=languageSelect.getFirstSelectedOption().getText();
        assertEquals("japanese",selectedLanguage);

        //change selected favourite category
        WebElement favourite = driver.findElement(By.name("account.favouriteCategoryId"));
        Select favouriteSelect = highLightSelect(favourite,"REPTILES");
        assertFalse(favouriteSelect.isMultiple());
        String selectedFavourite=favouriteSelect.getFirstSelectedOption().getText();
        assertEquals("REPTILES",selectedFavourite);

        //assert checkboxes are checked
        WebElement listOption = driver.findElement(By.name("account.listOption"));
        WebElement bannerOption = driver.findElement(By.name("account.bannerOption"));
        assertTrue(listOption.isSelected());
        assertTrue(bannerOption.isSelected());

        //deselect list option
        highlightClick(listOption);
        assertFalse(listOption.isSelected());
    }

    public void highlightClick(WebElement element){
        JavascriptExecutor js=(JavascriptExecutor) driver;
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        js.executeScript("arguments[0].setAttribute('style','background: yellow; border: 2px solid red;');",element);
        element.click();
    }

    public void highlightSendKeys(WebElement element, String value){
        JavascriptExecutor js=(JavascriptExecutor) driver;
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
        js.executeScript("arguments[0].setAttribute('style','background: yellow; border: 2px solid red;');",element);
        element.sendKeys(value);
    }

    public Select highLightSelect(WebElement element, String value){
        JavascriptExecutor js=(JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style','background: yellow; border: 2px solid red;');",element);
        Select select=new Select(element);
        select.selectByValue(value);
        return select;
    }


    @After
    public void tearDown(){
        driver.quit();
    }
}
