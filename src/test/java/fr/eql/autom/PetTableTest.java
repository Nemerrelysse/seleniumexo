package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PetTableTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test(){
        //String dog="Bulldog";
        String dog="Dalmation";

        //log in test
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
        WebElement singIn = driver.findElement(By.partialLinkText("Sign In"));
        singIn.click();
        WebElement passwordInput = driver.findElement(By.name("password"));
        passwordInput.clear();
        WebElement usernameInput = driver.findElement(By.name("username"));
        usernameInput.sendKeys("j2ee");
        passwordInput.sendKeys("j2ee");
        WebElement logIn = driver.findElement(By.name("signon"));
        logIn.click();
        WebElement signOut = driver.findElement(By.partialLinkText("Sign Out"));
        assertTrue(signOut.isDisplayed());
        WebElement welcome = driver.findElement(By.id("WelcomeContent"));
        assertTrue(welcome.isDisplayed());
        assertEquals("Welcome ABC!",welcome.getText());

        //go to dog category test
        goToCategory("dogs");

        //go to the choosen dog page
        WebElement cell=getCell(getRowIndex(dog),1);
        WebElement dogLink=cell.findElement(By.xpath("a"));
        dogLink.click();

        //dog page test
        WebElement title= driver.findElement(By.xpath("//div[@id='Catalog']/h2"));
        assertEquals(dog,title.getText());

    }

    @After
    public void tearDown(){
        driver.quit();
    }

    //go to a category (not case sensitive)
    public void goToCategory(String choice){
        String upperChoice=choice.toUpperCase();
        WebElement category = driver.findElement(By.xpath("//div[@id='SidebarContent']/a[@href='/actions/Catalog.action?viewCategory=&categoryId="+upperChoice+"']"));
        category.click();
        String listChoice=upperChoice.charAt(0)+choice.substring(1).toLowerCase();
        WebElement list = driver.findElement(By.xpath("//div[@id='Catalog']/h2[text()='"+listChoice+"']/following-sibling::table"));
        assertTrue(list.isDisplayed());
    }

    //get the first row that contains the string (case sensitive)
    public int getRowIndex(String s){
        int currentline=1;
        List<WebElement> rows = driver.findElements(By.xpath("//table/tbody/tr"));
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.xpath("td"));
            for (WebElement cell : cells) {
                if(cell.getText().equals(s)) {
                    return currentline;
                }
            }
            currentline++;
        }
        return -1;
    }

    //get the td cell at the given row and column (index starts at 1)
    public WebElement getCell(int row, int col){
        WebElement cell = driver.findElement(By.xpath("//table/tbody/tr["+row+"]/td["+col+"]"));
        return cell;
    }
}
