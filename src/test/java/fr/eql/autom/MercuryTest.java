package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class MercuryTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver=new ChromeDriver();
    }

    @Test
    public void test(){
        driver.get("https://demo.guru99.com/test/newtours/");
        driver.manage().window().maximize();

        //accept cookies
        driver.switchTo().frame("gdpr-consent-notice");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("save")));
        WebElement acceptCookie = driver.findElement(By.id("save"));
        acceptCookie.click();

        //go to login
        driver.switchTo().defaultContent();
        WebElement logIn = driver.findElement(By.partialLinkText("SIGN-ON"));
        logIn.click();

        //login
        WebElement userNameInput= driver.findElement(By.name("userName"));
        WebElement passwordInput= driver.findElement(By.name("password"));
        userNameInput.sendKeys("mercury");
        passwordInput.sendKeys("mercury");
        WebElement submitButton = driver.findElement(By.name("submit"));
        submitButton.click();

        //check login
        WebElement loginMessage = driver.findElement(By.xpath("//h3[text()='Login Successfully']"));
        assertTrue(loginMessage.isDisplayed());
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
