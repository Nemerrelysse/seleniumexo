package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HotelDateTest {
    WebDriver driver;
    String url = "http://localhost/TutorialHtml5HotelPhp";
    String resaName = "resa 1";
    int room=2;
    LocalDate today=LocalDate.now();
    LocalDate tomorrow=today.plusDays(1);
    DateTimeFormatter formatter= DateTimeFormatter.ofPattern("yyyy-MM-dd");


    @Before
    public void setUp(){
        //set the driver
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws InterruptedException {
        //locators
        By cellBy = getCell(1,1);
        By frameBy = By.xpath("//iframe");
        By nameInputBy = By.id("name");
        By startDateInputBy= By.id("start");
        By endDateInputBy= By.id("end");
        By roomInputBy=By.id("room");
        By saveButtonBy = By.xpath("//input[@value='Save']");
        By resaBy = By.xpath("//div[contains(text(),'"+resaName+"')]");
        By reservationBy = By.xpath("parent::div"); //from resa, not from driver
        By nextDayBy = getCell(room,tomorrow.getDayOfMonth());
        By updatedBy = By.xpath("//div[contains(text(),'Update successful')]");
        By closeBy = By.xpath("//div[contains(@class,'scheduler_default_event_delete')]");
        By deletedBy = By.xpath("//div[contains(text(),'Deleted')]");


        //click on the cell of today for the room
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement cell = driver.findElement(cellBy);
        cell.click();

        //switch to the reservation frame
        WebElement frame = driver.findElement(frameBy);
        assertTrue(frame.isDisplayed());
        driver.switchTo().frame(frame);

        //make a reservation
        WebElement nameInput = driver.findElement(nameInputBy);
        nameInput.sendKeys(resaName);
        String startDate=today.format(formatter)+"T12:00:00";
        System.out.println(startDate);
        String endDate=tomorrow.format(formatter)+"T12:00:00";
        System.out.println(endDate);
        WebElement startDateInput = driver.findElement(startDateInputBy);
        WebElement endDateInput = driver.findElement(endDateInputBy);
        startDateInput.clear();
        endDateInput.clear();
        WebElement roomInput=driver.findElement(roomInputBy);
        Select roomSelect = new Select(roomInput);
        roomSelect.selectByValue(""+room);
        startDateInput.sendKeys(startDate);
        endDateInput.sendKeys(endDate);
        WebElement saveButton = driver.findElement(saveButtonBy);
        saveButton.click();


        //check reservation is displayed
        driver.switchTo().defaultContent();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(resaBy));
        WebElement resa = driver.findElement(resaBy);
        assertTrue(resa.isDisplayed());

        //move reservation to the next day
        WebElement reservation=resa.findElement(reservationBy);
        WebElement nextDay = driver.findElement(nextDayBy);
        Actions a = new Actions(driver);
        a.dragAndDrop(reservation,nextDay).build().perform();

        //check if the update message appears and disappears
        wait.until(ExpectedConditions.presenceOfElementLocated(updatedBy));
        WebElement updated = driver.findElement(updatedBy);
        wait.until(ExpectedConditions.visibilityOf(updated));
        assertTrue(updated.isDisplayed());
        Thread.sleep(8000);
        assertFalse(updated.isDisplayed());

        //delete the reservation
        WebElement newResa = driver.findElement(nextDayBy);
        a.moveToElement(newResa).build().perform();
        wait.until(ExpectedConditions.presenceOfElementLocated(closeBy));
        WebElement close = driver.findElement(closeBy);
        close.click();

        //check that the deleted message appears and disappears
        wait.until(ExpectedConditions.presenceOfElementLocated(deletedBy));
        WebElement deleted = driver.findElement(deletedBy);
        wait.until(ExpectedConditions.visibilityOf(deleted));
        assertTrue(deleted.isDisplayed());
        Thread.sleep(8000);
        assertFalse(deleted.isDisplayed());
    }

    public By getCell(int row, int col){
        int topDelta = 50*(row-1);
        int leftDelta = 40*(col-1);
        String xpath = "//div[contains(@style,'left: "+leftDelta+"px; top: "+topDelta+"px; width: 40px; height: 50px;')]";
        System.out.println(xpath);
        return By.xpath(xpath);
    }


    @After
    public void tearDown(){
        driver.quit();
    }
}
