package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class HotelTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws InterruptedException {
        driver.get("http://localhost/TutorialHtml5HotelPhp");
        driver.manage().window().maximize();
        WebElement cell = driver.findElement(By.xpath("//*[@id=\"dp\"]/div[3]/div[3]/div/div[2]/div[1]"));
        cell.click();

        WebElement frame = driver.findElement(By.xpath("//iframe"));
        assertTrue(frame.isDisplayed());
        driver.switchTo().frame(frame);

        WebElement nameInput = driver.findElement(By.id("name"));
        nameInput.sendKeys("resa 1");

        WebElement saveButton = driver.findElement(By.xpath("//input[@value='Save']"));
        saveButton.click();

        driver.switchTo().defaultContent();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(text(),'resa 1')]")));
        WebElement resa = driver.findElement(By.xpath("//div[contains(text(),'resa 1')]"));
        assertTrue(resa.isDisplayed());
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
