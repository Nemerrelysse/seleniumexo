package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class HotelDragNDropTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void test() throws InterruptedException {
        driver.get("http://localhost/TutorialHtml5HotelPhp");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement cell = driver.findElement(By.xpath("//div[@style='left: 0px; top: 0px; width: 40px; height: 50px; position: absolute;']"));
        cell.click();

        WebElement frame = driver.findElement(By.xpath("//iframe"));
        assertTrue(frame.isDisplayed());
        driver.switchTo().frame(frame);

        WebElement nameInput = driver.findElement(By.id("name"));
        nameInput.sendKeys("resa 1");

        WebElement saveButton = driver.findElement(By.xpath("//input[@value='Save']"));
        saveButton.click();

        driver.switchTo().defaultContent();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(text(),'resa 1')]")));
        WebElement resa = driver.findElement(By.xpath("//div[contains(text(),'resa 1')]"));
        assertTrue(resa.isDisplayed());

        WebElement reservation=resa.findElement(By.xpath("parent::div"));
        WebElement nextDay = driver.findElement(By.xpath("//div[contains(@style,'left: 40px; top: 0px;')]"));
        Actions a = new Actions(driver);
        a.dragAndDrop(reservation,nextDay).build().perform();


        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(text(),'Update successful')]")));
        WebElement updated = driver.findElement(By.xpath("//div[contains(text(),'Update successful')]"));
        assertTrue(updated.isDisplayed());
        Thread.sleep(7000);
        assertFalse(updated.isDisplayed());

    }

    @After
    public void tearDown(){
        //driver.quit();
    }
}
