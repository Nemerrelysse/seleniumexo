package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;

public class Exercice1Test {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test(){
        driver.get("https://latavernedutesteur.fr");
        driver.manage().window().maximize();
        String title=driver.getTitle();
        System.out.println(title);
        assertEquals("Titles doesn't match","La taverne du testeur",title);
    }

    @After
    public void tearDown(){
        driver.quit();
    }
}
