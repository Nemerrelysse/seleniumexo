package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.*;

public class PetCheckboxTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test(){
        //log in test
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
        WebElement singIn = driver.findElement(By.partialLinkText("Sign In"));
        singIn.click();
        WebElement passwordInput = driver.findElement(By.name("password"));
        passwordInput.clear();
        WebElement usernameInput = driver.findElement(By.name("username"));
        usernameInput.sendKeys("j2ee");
        passwordInput.sendKeys("j2ee");
        WebElement logIn = driver.findElement(By.name("signon"));
        logIn.click();
        WebElement signOut = driver.findElement(By.partialLinkText("Sign Out"));
        assertTrue(signOut.isDisplayed());
        WebElement welcome = driver.findElement(By.id("WelcomeContent"));
        assertTrue(welcome.isDisplayed());
        assertEquals("Welcome ABC!",welcome.getText());

        //open my account
        WebElement myAccount = driver.findElement(By.partialLinkText("My Account"));
        myAccount.click();
        WebElement userInformation = driver.findElement(By.xpath("//h3[text()='User Information']"));
        assertTrue(userInformation.isDisplayed());

        //change selected language
        WebElement language = driver.findElement(By.name("account.languagePreference"));
        Select languageSelect = new Select(language);
        languageSelect.selectByValue("japanese");
        assertFalse(languageSelect.isMultiple());
        String selectedLanguage=languageSelect.getFirstSelectedOption().getText();
        assertEquals("japanese",selectedLanguage);

        //change selected favourite category
        WebElement favourite = driver.findElement(By.name("account.favouriteCategoryId"));
        Select favouriteSelect = new Select(favourite);
        favouriteSelect.selectByValue("REPTILES");
        assertFalse(favouriteSelect.isMultiple());
        String selectedFavourite=favouriteSelect.getFirstSelectedOption().getText();
        assertEquals("REPTILES",selectedFavourite);

        //assert checkboxes are checked
        WebElement listOption = driver.findElement(By.name("account.listOption"));
        WebElement bannerOption = driver.findElement(By.name("account.bannerOption"));
        assertTrue(listOption.isSelected());
        assertTrue(bannerOption.isSelected());

        //deselect list option
        listOption.click();
        assertFalse(listOption.isSelected());
    }
    @After
    public void tearDown(){
        driver.quit();
    }
}
