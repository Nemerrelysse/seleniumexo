package fr.eql.autom;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstDriverTest {
    @Test
    public void test(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://google.com");
    }
}
