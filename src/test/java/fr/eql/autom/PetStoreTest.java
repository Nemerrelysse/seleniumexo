package fr.eql.autom;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;

public class PetStoreTest {
    WebDriver driver;
    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","src/test/ressources/chromedriver.exe");
        driver = new ChromeDriver();
    }
    @Test
    public void test(){
        //log in test
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
        WebElement singIn = driver.findElement(By.partialLinkText("Sign In"));
        singIn.click();
        WebElement passwordInput = driver.findElement(By.name("password"));
        passwordInput.clear();
        WebElement usernameInput = driver.findElement(By.name("username"));
        usernameInput.sendKeys("j2ee");
        passwordInput.sendKeys("j2ee");
        WebElement logIn = driver.findElement(By.name("signon"));
        logIn.click();
        WebElement signOut = driver.findElement(By.partialLinkText("Sign Out"));
        assertTrue(signOut.isDisplayed());
        WebElement welcome = driver.findElement(By.id("WelcomeContent"));
        assertTrue(welcome.isDisplayed());
        assertEquals("Welcome ABC!",welcome.getText());

        //select fish category
        WebElement fish = driver.findElement(By.xpath("//div[@id='SidebarContent']/a[@href='/actions/Catalog.action?viewCategory=&categoryId=FISH']"));
        fish.click();
        WebElement fishList = driver.findElement(By.xpath("//div[@id='Catalog']/h2[text()='Fish']/following-sibling::table"));
        assertTrue(fishList.isDisplayed());

        //select a fish
        WebElement koi = driver.findElement(By.partialLinkText("FI-FW-01"));
        koi.click();

        //add fish to cart
        WebElement buyKoy = driver.findElement(By.xpath("//a[@href='/actions/Cart.action?addItemToCart=&workingItemId=EST-4']"));
        buyKoy.click();
        WebElement cart = driver.findElement(By.id("Cart"));
        assertTrue(cart.isDisplayed());

        //change quantity in cart
        WebElement quantity = driver.findElement(By.xpath("//input[@name='EST-4']"));
        quantity.clear();
        quantity.sendKeys("2");
        WebElement updateCart = driver.findElement(By.name("updateCartQuantities"));
        updateCart.click();

        //check that total price is correct
        WebElement itemPriceTable = driver.findElement(By.xpath("//input[@name='EST-4']/following::td"));
        WebElement totalPriceTable = driver.findElement(By.xpath("//input[@name='EST-4']/following::td/following::td"));
        String itemPriceString = itemPriceTable.getText().substring(1);
        String totalPriceString = totalPriceTable.getText().substring(1);
        double itemPrice = Double.parseDouble(itemPriceString);
        double totalPrice = Double.parseDouble(totalPriceString);
        assertEquals(2*itemPrice,totalPrice,0.001);


    }
    @After
    public void tearDown(){
        driver.quit();
    }
}
