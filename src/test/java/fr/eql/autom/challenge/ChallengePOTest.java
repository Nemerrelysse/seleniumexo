package fr.eql.autom.challenge;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ChallengePOTest {
    WebDriver driver;
    String url="https://www.rpachallenge.com/";
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","src/test/ressources/geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Test
    public void test() throws IOException {
        driver.get(url);
        driver.manage().window().maximize();


        //load jdd and start
        List<Map<String,String>> jdd=Tools.loadCSVJDD("challenge.csv");
        PageChallenge page = PageFactory.initElements(driver, PageChallenge.class);
        page.startChallenge();

        for(int i=0;i<10;++i) {
            //get values to input
            Map<String,String> line=jdd.get(i);
            page.fillForm(line);
        }
    }

    @After
    public void tearDown(){
        //driver.quit();
    }

    
}
