package fr.eql.autom.challenge;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChallengeLanguageTest {
    WebDriver driver;
    String url="https://www.rpachallenge.com/";
    String language="JA";
    @Before
    public void setUp(){
        System.setProperty("webdriver.gecko.driver","src/test/ressources/geckodriver.exe");
        driver = new FirefoxDriver();

    }

    @Test
    public void test() throws IOException {
        WebDriverWait wait=new WebDriverWait(driver,10);
        driver.get(url);
        driver.manage().window().maximize();

        //locators
        By startBy = By.xpath("//button");
        By roleBy = By.xpath("//input[@ng-reflect-name='labelRole']");
        By emailBy = By.xpath("//input[@ng-reflect-name='labelEmail']");
        By phoneBy = By.xpath("//input[@ng-reflect-name='labelPhone']");
        By lastNameBy = By.xpath("//input[@ng-reflect-name='labelLastName']");
        By adressBy = By.xpath("//input[@ng-reflect-name='labelAddress']");
        By companyBy = By.xpath("//input[@ng-reflect-name='labelCompanyName']");
        By firstNameBy = By.xpath("//input[@ng-reflect-name='labelFirstName']");
        By submitBy = By.xpath("//input[@type='submit']");
        By chooseLanguageBy = By.xpath("//a[@materialize='dropdown']");
        By langChoiceBy = By.xpath("//span[text()='"+language+"']");

        //load jdd
        List<Map<String,String>> jdd=loadCSVJDD("challenge.csv");
        Actions a=new Actions(driver);

        //select the language
        WebElement chooseLanguage = driver.findElement(chooseLanguageBy);
        a.moveToElement(chooseLanguage).build().perform();
        WebElement langChoice=driver.findElement(langChoiceBy);
        wait.until(ExpectedConditions.elementToBeClickable(langChoice));
        langChoice.click();
        WebElement start = driver.findElement(startBy);
        start.click();

        for(int i=0;i<10;++i) {
            //get the values to input
            Map<String,String> line=jdd.get(i);
            String role = line.get("Role in Company");
            String email = line.get("Email");
            String phone = line.get("Phone Number");
            String lastName = line.get("Last Name ");
            String address = line.get("Address");
            String company = line.get("Company Name");
            String firstName = line.get("First Name");

            //input the values
            WebElement roleInput = driver.findElement(roleBy);
            roleInput.sendKeys(role);

            WebElement emailInput = driver.findElement(emailBy);
            emailInput.sendKeys(email);

            WebElement phoneInput = driver.findElement(phoneBy);
            phoneInput.sendKeys(phone);

            WebElement lastNameInput = driver.findElement(lastNameBy);
            lastNameInput.sendKeys(lastName);

            WebElement adressInput = driver.findElement(adressBy);
            adressInput.sendKeys(address);

            WebElement companyInput = driver.findElement(companyBy);
            companyInput.sendKeys(company);

            WebElement firstNameInput = driver.findElement(firstNameBy);
            firstNameInput.sendKeys(firstName);

            //submit form
            WebElement submit = driver.findElement(submitBy);
            submit.click();
        }
    }

    @After
    public void tearDown(){
        //driver.quit();
    }

    public ArrayList<Map<String,String>> loadCSVJDD (String fileName) throws IOException {
        String csvFilePath ="src/test/ressources/jdd/"+fileName;
        ArrayList<Map<String,String>> listJDD = new ArrayList<>();
        List<String[]> list=
                Files.lines(Paths.get(csvFilePath))
                        .map(line ->line.split("\\\\r?\\\\n"))
                        .collect(Collectors.toList());
        String[] titres = list.get(0)[0].split(";");
        titres[0]=titres[0].replace("\uFEFF","");
        for (int i=1;i<list.size();++i){
            Map<String,String> jdd =new HashMap<>();
            String[] val =list.get(i)[0].split(";");
            for (int j=0; j<titres.length;++j){
                jdd.put(titres[j],val[j]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }
}
