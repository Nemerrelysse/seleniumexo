package fr.eql.autom.challenge;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Tools {
    public static ArrayList<Map<String,String>> loadCSVJDD (String fileName) throws IOException {
        String csvFilePath ="src/test/ressources/jdd/"+fileName;
        ArrayList<Map<String,String>> listJDD = new ArrayList<>();
        List<String[]> list=
                Files.lines(Paths.get(csvFilePath))
                        .map(line ->line.split("\\\\r?\\\\n"))
                        .collect(Collectors.toList());
        String[] titres = list.get(0)[0].split(";");
        titres[0]=titres[0].replace("\uFEFF","");
        for (int i=1;i<list.size();++i){
            Map<String,String> jdd =new HashMap<>();
            String[] val =list.get(i)[0].split(";");
            for (int j=0; j<titres.length;++j){
                jdd.put(titres[j],val[j]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }
}
