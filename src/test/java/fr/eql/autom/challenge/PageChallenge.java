package fr.eql.autom.challenge;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Map;

public class PageChallenge {
    @FindBy (xpath = "//button")
    WebElement start;
    @FindBy (xpath = "//input[@ng-reflect-name='labelRole']")
    WebElement roleInput;
    @FindBy (xpath = "//input[@ng-reflect-name='labelEmail']")
    WebElement emailInput;
    @FindBy (xpath = "//input[@ng-reflect-name='labelPhone']")
    WebElement phoneInput;
    @FindBy (xpath = "//input[@ng-reflect-name='labelLastName']")
    WebElement lastNameInput;
    @FindBy (xpath = "//input[@ng-reflect-name='labelAddress']")
    WebElement addressInput;
    @FindBy (xpath = "//input[@ng-reflect-name='labelCompanyName']")
    WebElement companyInput;
    @FindBy (xpath = "//input[@ng-reflect-name='labelFirstName']")
    WebElement firstNameInput;
    @FindBy (xpath = "//input[@type='submit']")
    WebElement submit;
    String role;
    String email;
    String phone;
    String lastName;
    String address;
    String company;
    String firstName;

    public void setValues(Map<String,String> line){
        role = line.get("Role in Company");
        email = line.get("Email");
        phone = line.get("Phone Number");
        lastName = line.get("Last Name ");
        address = line.get("Address");
        company = line.get("Company Name");
        firstName = line.get("First Name");
    }

    public void inputs(){
        roleInput.sendKeys(role);
        emailInput.sendKeys(email);
        phoneInput.sendKeys(phone);
        lastNameInput.sendKeys(lastName);
        addressInput.sendKeys(address);
        companyInput.sendKeys(company);
        firstNameInput.sendKeys(firstName);
    }

    public void fillForm(Map<String,String> line){
        setValues(line);
        inputs();
        submit.click();
    }

    public void startChallenge(){
        start.click();
    }

}
